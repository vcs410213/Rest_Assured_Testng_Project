Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Tue, 30 Apr 2024 10:46:24 GMT
Content-Type=application/json; charset=utf-8
Content-Length=82
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714473984&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=rq%2F3o7hPlL02npK0wnDmIOZRTlf%2BPFbV7iyvlejW%2B60%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714473984&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=rq%2F3o7hPlL02npK0wnDmIOZRTlf%2BPFbV7iyvlejW%2B60%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"52-JG7XX2v5cA/IzZ3sq+BRPK8VCIk"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87c6f29f7a0b7fb3-MAA

Response body is :
{"name":"anand","job":"maneger","id":"638","createdAt":"2024-04-30T10:46:24.169Z"}