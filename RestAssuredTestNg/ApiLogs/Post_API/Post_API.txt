Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Sun, 26 May 2024 13:48:00 GMT
Content-Type=application/json; charset=utf-8
Content-Length=84
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1716731280&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=cBv%2Bm%2BqJ1CV%2FVnTfO%2B0wb056PWDYieemNZdjUyhrquc%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1716731280&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=cBv%2Bm%2BqJ1CV%2FVnTfO%2B0wb056PWDYieemNZdjUyhrquc%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"54-yr/9jafhEVZQpX50Jc2ImPexSYU"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=889e3867d89184b3-BOM

Response body is :
{"name":"morpheus","job":"leader","id":"395","createdAt":"2024-05-26T13:48:00.813Z"}