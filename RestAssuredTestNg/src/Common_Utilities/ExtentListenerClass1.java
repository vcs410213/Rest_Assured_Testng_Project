package Common_Utilities;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass1 implements ITestListener {

	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
	ExtentTest test;

	public void reportConfiguration() {
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
		extentReport = new ExtentReports();

		extentReport.attachReporter(sparkReporter);

		extentReport.setSystemInfo("OS", "Windows 11");
		extentReport.setSystemInfo("User", "shubh");

		sparkReporter.config().setDocumentTitle("RestAssured Extent Listener Report");
		sparkReporter.config().setReportName("This is my first Extent Report");
		sparkReporter.config().setTheme(Theme.DARK);
	}

	@Override
	public void onStart(ITestContext result) {
		reportConfiguration();
		System.out.println("On Start method invoked...");
	}

	@Override
	public void onFinish(ITestContext result) {
		System.out.println("On Finish method invoked...");
		extentReport.flush();
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of the test method failed: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.FAIL,
				MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(), ExtentColor.RED));
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of the test method skipped: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP,
				MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(), ExtentColor.YELLOW));
	}

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Name of the test method started: " + result.getName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of the test method executed successfully: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the passed test case is: " + result.getName(), ExtentColor.GREEN));
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
	}
}
/*import org.testng.ITestContext;
import org.testng.ITestListener;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass1 implements ITestListener {
	
	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
	ExtentTest test;
	
	public void reportConfigurations() {
		sparkReporter=new ExtentSparkReporter(".\\extent-report\\report.html");
		extentReport=new ExtentReports();
		
		extentReport.attachReporter(sparkReporter);
		extentReport.setSystemInfo("OS","Windows 11");
		extentReport.setSystemInfo("user","shubh");
		
		 sparkReporter.config().setDocumentTitle("Rest Assured Extent Listener Report");
		 
		 sparkReporter.config().setReportName("This is my first Extent-Report");
		 sparkReporter.config().setTheme(Theme.DARK);
		
		
	}
	
	
	public void onStart(ITestContext result) {
		 reportConfigurations();
		System.out.println("on start method invoked....");
	}

	public void onFinish(ITestContext result) {
		System.out.println("on finish method invoked....");
		 extentReport.flush();
	}
	
	public void onTestFailure(ITestContext result) {
		System.out.println("name of test method failed:" +result.getName());
		test= extentReport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the Failed test case is: " +result.getName(),ExtentColor.RED));
		
	}
	
	public void onTestSkipped(ITestContext result) {
		System.out.println("name of test method skipped:" +result.getName());
		test= extentReport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the skipped test case is: " +result.getName(),ExtentColor.YELLOW));
		
	}
	
	public void onTestStart(ITestContext result) {
		System.out.println("name of test method started:" +result.getName());
	}
	
	public void onTestSuccess(ITestContext result) {
		System.out.println("name of test method executed successfully:" +result.getName());
		test= extentReport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the Passed test case is: " +result.getName(),ExtentColor.GREEN));
		
	}
	public void onTestfailedButWithinSuccessPercentage(ITestContext result) {
		
	}
	

}*/

