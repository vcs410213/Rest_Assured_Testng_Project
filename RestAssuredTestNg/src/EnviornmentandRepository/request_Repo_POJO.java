package EnviornmentandRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import POJO.POJO_Patch_API;
import POJO.POJO_Post_API;
import POJO.POJO_Put_API;

public class request_Repo_POJO {

	ObjectMapper OM = new ObjectMapper();
	POJO_Post_API postpojoobj = new POJO_Post_API();

	public String postTC1() throws JsonProcessingException {
		postpojoobj.setName("morpheus");
		postpojoobj.setJob("dev");

		String requestBody = OM.writeValueAsString(postpojoobj);
		return requestBody;

	}

	POJO_Put_API putpojoobj = new POJO_Put_API();

	public String putTC2() throws JsonProcessingException {
		putpojoobj.setName("morpheus");
		putpojoobj.setJob("tester");

		String requestBody = OM.writeValueAsString(putpojoobj);
		return requestBody;

	}
	
	POJO_Patch_API obj=new POJO_Patch_API();
	
	public String patchTC3() throws JsonProcessingException {
		obj.setName("morpheus");
		obj.setJob("QAEngineer");
		
		String requestBody=OM.writeValueAsString(obj);
		return requestBody;
		
		
	}

}
