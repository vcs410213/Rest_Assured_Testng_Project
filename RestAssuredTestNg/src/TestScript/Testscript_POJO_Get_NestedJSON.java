package TestScript;

import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import Common_Methods.Utilities;
import POJO.POJO_GetAPI_DataArray;
import POJO.POJO_GetAPI_Parent;

import java.io.File;
import Common_Methods.API_Trigger;
import Common_Methods.Testng_Retry_Analyzer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Testscript_POJO_Get_NestedJSON extends API_Trigger {

	File logfolder;
	Response response;
	POJO_GetAPI_Parent responseBody;

	@BeforeTest
	public void Setup() {
		logfolder = Utilities.createFolder("Get_API");
	}

	@Test (retryAnalyzer=Testng_Retry_Analyzer.class, description="validate the response body parameters of Get_TC1 ")
   public void validate_get() {

		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		String[] email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		int[] id = { 7, 8, 9, 10, 11, 12 };
		String[] first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		response = Get_API_Trigger(get_request_body(), get_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.as(POJO_GetAPI_Parent.class);

		List<POJO_GetAPI_DataArray> dataArray = responseBody.getData();
		int size = dataArray.size();


		for (int i = 0; i < size ; i++) {

			// String exp_id = Integer.toString(id[i]);
			// System.out.println(exp_id);
			int exp_id = id[i];
			System.out.println(exp_id);

			String exp_email_id = email[i];
			System.out.println(exp_email_id);

			String exp_first_name = first_name[i];
			System.out.println(exp_first_name);
			String exp_last_name = last_name[i];
			System.out.println(exp_last_name);

			String exp_avatar = avatar[i];
			System.out.println(exp_avatar);

			// String res_id = jsp_res.getString("data["+i+"].id");
			int res_id = dataArray.get(i).getId();
			String res_email = dataArray.get(i).getEmail();
			String res_first_name =dataArray.get(i).getFirst_name();
			String res_last_name =dataArray.get(i).getLast_name();
					
			String res_avatar =dataArray.get(i).getAvatar();
			int res_page = responseBody.getPage();
			int res_per_page = responseBody.getPer_page();
			int res_total = responseBody.getTotal();
			int res_total_pages = responseBody.getTotal_pages();
			
			Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

			Assert.assertEquals(res_page, exp_page);
			Assert.assertEquals(res_per_page, exp_per_page);
			Assert.assertEquals(res_total, exp_total);
			Assert.assertEquals(res_total_pages, exp_total_pages);

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email_id);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
			Assert.assertEquals(res_avatar, exp_avatar);
			// Step 7.3 : Validate support json
			Assert.assertEquals(responseBody.getSupport().getUrl(), exp_url, "Validation of URL failed");
			Assert.assertEquals(responseBody.getSupport().getText(), exp_text,
					"Validation of text failed");
		}
	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Get_API_TC4", logfolder, get_endpoint(), get_request_body(),
				response.getHeaders().toString(), response.getBody().asString());

	}

}
