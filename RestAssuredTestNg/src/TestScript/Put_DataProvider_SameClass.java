package TestScript;

import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;

import Common_Methods.Utilities;
import java.io.File;
import Common_Methods.API_Trigger;
import Common_Methods.Testng_Retry_Analyzer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_DataProvider_SameClass extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;
	
	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] { { "morpheus", "zion resident" }, { "shubhada", "developer" } };
	}


	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Put_API");
	}
   
	@Test (retryAnalyzer=Testng_Retry_Analyzer.class, dataProvider="requestBody",description="validate the response body parameters of Put_TC1 ")
	public void validate_put(String req_name,String req_job) {
    	String requestBody ="{\r\n"+ "    \"name\": \""+req_name+"\",\r\n"+ "    \"job\": \""+req_job+" \"\r\n"+ "}";  
		response = Put_API_Trigger(requestBody, put_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		

		JsonPath jsp_req = new JsonPath(put_request_body());
		String Req_name = jsp_req.getString("name");
		String Req_job = jsp_req.getString("job");

		
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Put_API_TC2", logfolder, put_endpoint(), put_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}
}
