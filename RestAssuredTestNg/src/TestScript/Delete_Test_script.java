package TestScript;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Testng_Retry_Analyzer;
import Common_Methods.Utilities;
import io.restassured.response.Response;
//import io.restassured.response.Response.getHeader;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseBodyData;

public class Delete_Test_script extends API_Trigger {

	File logfolder;
	Response response;
	

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Delete_API");
	}

	@Test (retryAnalyzer=Testng_Retry_Analyzer.class, description="validate the response body parameters of Delete_TC1 ")

	public void validate_delete() {

		response = API_Trigger.Delete_API_Trigger(delete_request_body(), delete_endpoint());
		System.out.println(response);
		
		int statuscode = response.statusCode();
		System.out.println(statuscode);
		System.out.println(response.getHeaders());
	
		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}

	@AfterTest
	public void teardown() throws IOException {
	
		Utilities.createLogFile("Delete_API_TC5", logfolder, delete_endpoint(), delete_request_body(),
				response.getHeaders().toString(), response.asString());

	}

}