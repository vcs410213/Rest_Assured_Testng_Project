package TestScript;

import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

import Common_Methods.Utilities;
import EnviornmentandRepository.request_Repo_POJO;
import POJO.POJO_Patch_API;

import java.io.File;
import Common_Methods.API_Trigger;
import Common_Methods.Testng_Retry_Analyzer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestScript_POJO_Patch extends API_Trigger {
	File logfolder;
	Response response;
	
	request_Repo_POJO obj=new request_Repo_POJO();
	

	@BeforeTest
	public void Setup() {
		logfolder = Utilities.createFolder("Patch_API");
	}

	@Test (retryAnalyzer=Testng_Retry_Analyzer.class, description="validate the response body parameters of Patch_TC1 ")
	public void validate_patch() throws JsonProcessingException {
		response = Patch_API_Trigger(obj.patchTC3(), patch_endpoint());
		int statuscode = response.statusCode();
		POJO_Patch_API responseBody=response.as(POJO_Patch_API.class);
		String res_name = responseBody.getName();
		String res_job = responseBody.getJob();
		String res_updatedAt = responseBody.getUpdatedAt();
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(obj.patchTC3());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Patch_API_TC3", logfolder, patch_endpoint(), patch_request_body(),
				response.getHeaders().toString(), response.getBody().asString());
	}

}
